
// Função principal para buscar e listar json
let getJson = ( ) => {

    let file = './assets/files/dados.json';
    let request = new XMLHttpRequest();
    
    request.open('GET', file, true);
    request.onload = () => {

        if (request.status >= 200 && request.status < 400) {

            let data = JSON.parse(request.responseText);       
            let total = data.length;
            let boxes = document.querySelectorAll('.Boxes-item').length;
            let create = total - boxes;

            // Cria demais boxes
            if (createBox(create)) {

                createLinks();
                // Faz o looping
                Array.prototype.forEach.call(data, function (el, i) {
                    //console.log(el, i);
                    addBoxes(el, i);
                });
            }                 

        } else {
            error();
        }
    };

    request.onerror = () => {
        error();
    };

    request.send();
}


let error = () => {
    return 'Ocorreu um erro.';
}

let createBox = ( num ) => {
    
    let box = document.getElementsByClassName('Boxes-item');
    let main = document.getElementById('Boxes');

    for (let index = 0; index < num; index++) {
        let clone = box[0].cloneNode(true);
        main.appendChild(clone);
    }
    return true;
}

let addBoxes = ( data, index ) => {

    // Lista todos os elemtentos criados
    let box = document.getElementsByClassName('Boxes-item');
    let boxText = box[index].getElementsByClassName('Boxes-text');    
    let boxImg = box[index].getElementsByClassName('Boxes-photo-img');
    let boxCod = box[index].getElementsByClassName('Boxes-cod');

    // Busca os campos especificos
    let id = boxCod[0].getElementsByTagName('span')[0];
    let nome = boxText[0].getElementsByTagName('strong')[0];
    let cargo = boxText[0].getElementsByTagName('p')[0];
    let foto = boxText[0].getElementsByTagName('strong')[0];

    // Criando a imagem
    let img = document.createElement('img');
    img.setAttribute("src", "./assets/img/" + data.foto);
    img.setAttribute("alt", data.nome);    
    boxImg[0].append(img);

    // Setando id
    box[index].setAttribute('data-id', data.id);

    // Limpa os campos caso exista alguma informação
    nome.innerHTML = '';
    cargo.innerHTML = '';
    id.innerHTML = '';  
   
    // Incluindo campos        
    nome.append( data.nome );
    cargo.append( data.cargo );
    id.append( data.id );    

}

let createLinks = () => {

    let boxes = document.querySelectorAll(".Boxes-item");
    let i = 0, length = boxes.length;

    for (i; i < length; i++) {
        if (document.addEventListener) {
            boxes[i].addEventListener("click", function () {
                let id = this.getAttribute('data-id');

                getBox(id );
            });
        } else {
            boxes[i].attachEvent("onclick", function () {
                let id = this.getAttribute('data-id');
                getBox( id );
            });
        };
    };



}

let addBox = (data) => {

    let nome = document.getElementById('Nome');
    let cargo = document.getElementById('Cargo');
    let idade = document.getElementById('Idade');
    let foto = document.getElementById('Foto');

    // Limpa os campos caso exista alguma informação
    nome.innerHTML = '';
    cargo.innerHTML = '';
    idade.innerHTML = '';
    foto.innerHTML = '';

    // Criando a imagem
    let img = document.createElement('img');
    img.setAttribute("src", "./assets/img/" + data.foto);
    img.setAttribute("alt", data.nome);
    foto.append(img);  

    // Incluindo campos        
    nome.append(data.nome);
    cargo.append(data.cargo);
    idade.append(data.idade);

    return true;

}

let getBox = ( id ) => {

    let main = document.getElementById('Box');
    window.location.hash = '#' + id;

    main.classList.add('is-loading');

    if(!main.classList.contains('is-view')){

        setTimeout(() => {
            main.classList.add('is-view');            
        }, 200);    

    }   
    
    setTimeout(() => {        
      
        let file = './assets/files/dados.json';
        let request = new XMLHttpRequest();

        request.open('GET', file, true);
        request.onload = () => {

            if (request.status >= 200 && request.status < 400) {

                let data = JSON.parse(request.responseText);

                Array.prototype.forEach.call(data, function (el, i) {
                    
                    if(el.id == id){

                        if (addBox(el)){

                            setTimeout(() => {                            
                                main.classList.remove('is-loading');                  
                            }, 1000); 

                        }

                    }
                    
                });
            

            } else {
                error();
            }
        };

        request.onerror = () => {
            error();
        };

        request.send();
    }, 400); 


}

let init = () => {

    menu();

    if (window.location.hash) {
        
        var hash = window.location.hash.slice(1);
        
        getBox( hash );

    }

    
}

let openMenu = () => {

    let header = document.getElementById('Header');

    if (header.classList.contains('is-open')) {
        header.classList.remove('is-open');
    } else {
        header.classList.add('is-open');
    }

}

let menu = () => {

    document.getElementById("BtnOpen").addEventListener("click", openMenu);

    
}

setTimeout(() => {
    getJson();    
}, 500);

init();


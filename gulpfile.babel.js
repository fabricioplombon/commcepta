'use strict';

import gulp from 'gulp';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import del from 'del';
import uglify from 'gulp-uglify';
import sass from 'gulp-sass';
import cleanCSS from 'gulp-clean-css';
import rename from 'gulp-rename';
import browserSync from 'browser-sync';
import imagemin from 'gulp-imagemin';

const server = browserSync.create();

const paths = {
    styles: {
        src: 'src/styles/**/*.scss',
        dest: 'assets/css/'
    },
    scripts: {
        src: 'src/scripts/**/*.js',
        dest: 'assets/js/'
    },
    images: {
        src: 'src/images/**/*.{gif,jpg,png,svg}',
        dest: 'assets/img/'
    },
    html: './*.{html,php}',
};

export const clean = () => del(['assets/css/', 'assets/js']);

export function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(rename({
            basename: 'main',
            suffix: '.min'
        }))
        .pipe(gulp.dest(paths.styles.dest));
}

export function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest(paths.scripts.dest));
}

export function images() {
    return gulp.src(paths.images.src, { since: gulp.lastRun(images) })
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(paths.images.dest));
}

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    server: {
      baseDir: './'
    }
  });
  done();
}

//const watch = () => gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
const watch = () => {
    gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
    gulp.watch(paths.styles.src, gulp.series(styles, reload));
    gulp.watch(paths.html, reload);
    gulp.watch(paths.images.src, gulp.series(images, reload));
}

const dev = gulp.series(serve, gulp.parallel(scripts, styles, images));
// const build = gulp.series(clean, scripts, serve, watch);
const build = gulp.series(clean, scripts, styles, serve, watch);
export default build;